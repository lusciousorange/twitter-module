<?php
class TMv_TwitterUserEmbeddedFeed extends TCv_View
{
	use TMt_PagesContentView;
	
	protected $handle = '';
	protected $num_tweets;
	


	
	public function html()
	{
		if($this->handle != '')
		{
			$this->handle = str_ireplace('@', '', $this->handle);

			$timeline_link = new TCv_Link();
			$timeline_link->addClass("twitter-timeline");
			$timeline_link->setURL("https://twitter.com/" . $this->handle);
			$timeline_link->addText('Tweets by @' . $this->handle );
			$timeline_link->setAttribute('data-tweet-limit', $this->num_tweets);
			$timeline_link->setAttribute('nofooter','nofooter');
			$this->attachView($timeline_link);

		}
		$this->addJSFile('twitter_widgets', "//platform.twitter.com/widgets.js");

		return parent::html();
	}


//<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="7" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:34.81481481481482% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BStmcDOgkAx/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">PUBLIC WAY OF THE CROSS Friday, April 14, 2017 9:30 a.m. Alumnae Hall, St. Mary&#39;s Academy, 550 Wellington Crescent There are 6 Stations of the Cross along a 1.5 km route traversing from SMA to Wellington Crescent, Grosvenor Street, Ruskin Row, Kingsway Avenue, Stafford Street and back to SMA. Archbishop Richard Gagnon, along with members of the SMA community will lead the procession. @oyyamwinnipeg  @archdioceseofwinnipeg #PublicWayoftheCross</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A post shared by St. Mary&#39;s Academy (@smawinnipeg) on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-04-10T17:03:03+00:00">Apr 10, 2017 at 10:03am PDT</time></p></div></blockquote>
//<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>


	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////


	/**
	 * Editor Form Items for the content form
	 * @return array
	 */
	public function pageContent_EditorFormItems()
	{
		$form_items = array();
	
		$field = new TCv_FormItem_TextField('handle', 'Twitter User Handle @');
		$field->setHelpText('The Twitter handle to be embedded. ');
		$form_items[] = $field;

		$field = new TCv_FormItem_Select('num_tweets', 'Num Tweets');
		$field->setHelpText('The number of tweets to be shown. ');
		$field->setOptionsWithNumberRange(1, 20);
		$field->setDefaultValue('3');
		$form_items[] = $field;

		return $form_items;
	}
	
	
	public static function pageContent_ViewTitle() { return 'Twitter User Timeline'; }
	public static function pageContent_IconCode() { return 'fa-twitter'; }
	public static function pageContent_ViewDescription()
	{ 
		return 'An embedded timeline for a Twitter User.';
	}
	
	public static function pageContent_ShowPreviewInBuilder()
	{
		return false;
	}
	
}

?>